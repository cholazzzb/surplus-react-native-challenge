import { Image } from 'expo-image';
import { useState, useCallback } from 'react';
import { Image as RNImage, ScrollView, StyleSheet, View } from 'react-native';
import { ActivityIndicator, Button, Modal, Text } from 'react-native-paper';

import { ImageLogin } from '~assets/__generated__/assetsImage';
import SafeAreaWrapper from '~components/SafeAreaWrapper';
import Wrapper from '~features/auth/components/LoginWrapper';

const imageUrl = RNImage.resolveAssetSource(ImageLogin);
const aspectRatio = imageUrl.width / imageUrl.height;

type ModalType = 'loading' | 'error' | null;
export default function LoginScreen() {
  const [modalType, setModalType] = useState<ModalType>(null);
  const onOpenModalLoading = () => setModalType('loading');
  const onOpenModalError = () => setModalType('error');
  const onCloseModal = () => setModalType(null);

  const getModalContainerStyle = useCallback((modalType: ModalType) => {
    switch (modalType) {
      case 'loading': {
        return styles.modalContainerLoading;
      }
      case 'error': {
        return styles.modalContainerError;
      }
      default: {
        return {};
      }
    }
  }, []);

  const getModalStyle = useCallback((modalType: ModalType) => {
    switch (modalType) {
      case 'loading': {
        return styles.modalLoading;
      }
      case 'error': {
        return styles.modalError;
      }
      default: {
        return {};
      }
    }
  }, []);

  const renderModal = useCallback((modalType: ModalType) => {
    switch (modalType) {
      case 'loading': {
        return <ActivityIndicator animating />;
      }
      case 'error': {
        return (
          <>
            <Text>INFORMASI</Text>
            <Text>Email atau password salah.</Text>
            <Button mode="contained-tonal" onPress={onCloseModal}>
              <Text>OK</Text>
            </Button>
          </>
        );
      }
      default: {
        return <></>;
      }
    }
  }, []);
  return (
    <SafeAreaWrapper>
      <View style={styles.container}>
        <ScrollView>
          <Image source={ImageLogin} style={styles.image} />

          <Wrapper
            onOpenModalLoading={onOpenModalLoading}
            onOpenModalError={onOpenModalError}
            onCloseModal={onCloseModal}
          />
        </ScrollView>
      </View>
      <Modal
        visible={!!modalType}
        onDismiss={onCloseModal}
        contentContainerStyle={getModalContainerStyle(modalType)}>
        <View style={getModalStyle(modalType)}>{renderModal(modalType)}</View>
      </Modal>
    </SafeAreaWrapper>
  );
}
const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    justifyContent: 'flex-end',
    backgroundColor: 'green',
  },
  image: {
    contentFit: 'contain',
    aspectRatio: aspectRatio,
  },
  modalContainerLoading: {
    alignItems: 'center',
  },
  modalLoading: {
    backgroundColor: 'white',
    width: 40,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalContainerError: {
    margin: 20,
    backgroundColor: 'white',
  },
  modalError: {
    borderRadius: 20,
    padding: 20,
    alignItems: 'center',
  },
});
