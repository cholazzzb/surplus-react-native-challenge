import { ScrollView, StyleSheet } from 'react-native';
import { Button, List, Surface, useTheme } from 'react-native-paper';

import SafeAreaWrapper from '~components/SafeAreaWrapper';
import { useAuth } from '~features/auth/provider';

export default function EditAccount() {
  const { logout } = useAuth();
  const theme = useTheme();

  return (
    <SafeAreaWrapper style={styles.safeArea}>
      <ScrollView>
        <List.Item
          title="Data Pribadi"
          description="Atur nama, nomor telepon, dan alamat email"
          right={(props) => <List.Icon {...props} icon="chevron-right" />}
        />
        <List.Item
          title="Alamat Favorit"
          description="Atur alamat favorit kamu"
          right={(props) => <List.Icon {...props} icon="chevron-right" />}
        />
        <List.Item
          title="Kata Sandi"
          description="......."
          right={(props) => <List.Icon {...props} icon="chevron-right" />}
        />
        <List.Item
          title="Hapus AKun"
          description="Nama, badges, voucher dan profil akan hilang kektika kamu menyetujui untuk menghapus akun"
          right={(props) => <List.Icon {...props} icon="chevron-right" />}
        />
      </ScrollView>
      <Surface style={styles.buttonBottom}>
        <Button
          onPress={logout}
          mode="contained"
          buttonColor={theme.colors.error}>
          Keluar
        </Button>
      </Surface>
    </SafeAreaWrapper>
  );
}

const styles = StyleSheet.create({
  safeArea: {
    flexGrow: 1,
    flexDirection: 'column',
    justifyContent: 'flex-end',
  },
  buttonBottom: {
    padding: 20,
  },
});
