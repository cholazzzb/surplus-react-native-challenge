import { Image } from 'expo-image';
import { useCallback, useState } from 'react';
import { Image as RNImage, ScrollView, StyleSheet, View } from 'react-native';
import { ActivityIndicator, Button, Modal, Text } from 'react-native-paper';

import { ImageRegister } from '~assets/__generated__/assetsImage';
import SafeAreaWrapper from '~components/SafeAreaWrapper';
import RegisterWrapper from '~features/auth/components/RegisterWrapper';

const imageUrl = RNImage.resolveAssetSource(ImageRegister);
const aspectRatio = imageUrl.width / imageUrl.height;

type ModalType = 'loading' | 'error' | null;
export default function WelcomeScreen() {
  const [modalType, setModalType] = useState<ModalType>(null);
  const onOpenModalLoading = () => setModalType('loading');
  const onOpenModalError = () => setModalType('error');
  const onCloseModal = () => setModalType(null);

  const getModalContainerStyle = useCallback((modalType: ModalType) => {
    switch (modalType) {
      case 'loading': {
        return styles.modalContainerLoading;
      }
      case 'error': {
        return styles.modalContainerError;
      }
      default: {
        return {};
      }
    }
  }, []);

  const getModalStyle = useCallback((modalType: ModalType) => {
    switch (modalType) {
      case 'loading': {
        return styles.modalLoading;
      }
      case 'error': {
        return styles.modalError;
      }
      default: {
        return {};
      }
    }
  }, []);

  const renderModal = useCallback((modalType: ModalType) => {
    switch (modalType) {
      case 'loading': {
        return <ActivityIndicator animating />;
      }
      case 'error': {
        return (
          <>
            <Text>INFORMASI</Text>
            <Text>Email sudah digunakan</Text>
            <Button mode="contained-tonal" onPress={onCloseModal}>
              <Text>OK</Text>
            </Button>
          </>
        );
      }
      default: {
        return <></>;
      }
    }
  }, []);
  return (
    <SafeAreaWrapper>
      <View style={styles.container}>
        <ScrollView>
          <Image source={ImageRegister} style={styles.image} />

          <RegisterWrapper
            onOpenModalLoading={onOpenModalLoading}
            onOpenModalError={onOpenModalError}
            onCloseModal={onCloseModal}
          />
        </ScrollView>

        <Modal
          visible={!!modalType}
          onDismiss={onCloseModal}
          contentContainerStyle={getModalContainerStyle(modalType)}>
          <View style={getModalStyle(modalType)}>{renderModal(modalType)}</View>
        </Modal>
      </View>
    </SafeAreaWrapper>
  );
}

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    justifyContent: 'flex-end',
    backgroundColor: 'green',
  },
  image: {
    contentFit: 'contain',
    aspectRatio: aspectRatio,
  },
  modalContainerLoading: {
    alignItems: 'center',
  },
  modalLoading: {
    backgroundColor: 'white',
    width: 40,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalContainerError: {
    margin: 20,
    backgroundColor: 'white',
  },
  modalError: {
    borderRadius: 20,
    padding: 20,
    alignItems: 'center',
  },
});
