import { Image } from 'expo-image';
import { Image as RNImage, ScrollView, StyleSheet, View } from 'react-native';

import { ImageWelcome } from '~assets/__generated__/assetsImage';
import SafeAreaWrapper from '~components/SafeAreaWrapper';
import Wrapper from '~features/welcome/components/Wrapper';

const imageUrl = RNImage.resolveAssetSource(ImageWelcome);
const aspectRatio = imageUrl.width / imageUrl.height;

export default function WelcomeScreen() {
  return (
    <SafeAreaWrapper>
      <View style={styles.container}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <Image source={ImageWelcome} style={styles.image} />

          <Wrapper />
        </ScrollView>
      </View>
    </SafeAreaWrapper>
  );
}

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    justifyContent: 'flex-end',
    backgroundColor: 'green',
  },
  image: {
    contentFit: 'contain',
    aspectRatio: aspectRatio,
  },
});
