import FontAwesome from '@expo/vector-icons/FontAwesome';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import { useFonts } from 'expo-font';
import { Link, SplashScreen, Stack, useRouter } from 'expo-router';
import { StatusBar } from 'expo-status-bar';
import { useEffect } from 'react';
import { Platform } from 'react-native';
import {
  Button,
  MD3LightTheme as DefaultTheme,
  IconButton,
  Provider as PaperProvider,
} from 'react-native-paper';

import { AuthProvider } from '~features/auth/provider';

export { ErrorBoundary } from 'expo-router';

export const unstable_settings = {
  // Ensure that reloading on `/modal` keeps a back button present.
  initialRouteName: 'index',
};

export default function RootLayout() {
  const [loaded, error] = useFonts({
    SpaceMono: require('../assets/fonts/SpaceMono-Regular.ttf'),
    ...FontAwesome.font,
  });

  // Expo Router uses Error Boundaries to catch errors in the navigation tree.
  useEffect(() => {
    if (error) throw error;
  }, [error]);

  return (
    <>
      {/* Keep the splash screen open until the assets have loaded. In the future, we should just support async font loading with a native version of font-display. */}
      {!loaded && <SplashScreen />}
      {loaded && <RootLayoutNav />}
    </>
  );
}

const theme = {
  ...DefaultTheme,
  colors: {
    primary: 'rgb(0, 107, 94)',
    onPrimary: 'rgb(255, 255, 255)',
    primaryContainer: 'rgb(118, 248, 225)',
    onPrimaryContainer: 'rgb(0, 32, 27)',
    secondary: 'rgb(74, 99, 94)',
    onSecondary: 'rgb(255, 255, 255)',
    secondaryContainer: 'rgb(205, 232, 225)',
    onSecondaryContainer: 'rgb(6, 32, 27)',
    tertiary: 'rgb(68, 98, 121)',
    onTertiary: 'rgb(255, 255, 255)',
    tertiaryContainer: 'rgb(202, 230, 255)',
    onTertiaryContainer: 'rgb(0, 30, 48)',
    error: 'rgb(186, 26, 26)',
    onError: 'rgb(255, 255, 255)',
    errorContainer: 'rgb(255, 218, 214)',
    onErrorContainer: 'rgb(65, 0, 2)',
    background: 'rgb(250, 253, 250)',
    onBackground: 'rgb(25, 28, 27)',
    surface: 'rgb(250, 253, 250)',
    onSurface: 'rgb(25, 28, 27)',
    surfaceVariant: 'rgb(218, 229, 225)',
    onSurfaceVariant: 'rgb(63, 73, 70)',
    outline: 'rgb(111, 121, 118)',
    outlineVariant: 'rgb(190, 201, 197)',
    shadow: 'rgb(0, 0, 0)',
    scrim: 'rgb(0, 0, 0)',
    inverseSurface: 'rgb(45, 49, 48)',
    inverseOnSurface: 'rgb(239, 241, 239)',
    inversePrimary: 'rgb(86, 219, 197)',
    elevation: {
      level0: 'transparent',
      level1: 'rgb(238, 246, 242)',
      level2: 'rgb(230, 241, 238)',
      level3: 'rgb(223, 237, 233)',
      level4: 'rgb(220, 236, 231)',
      level5: 'rgb(215, 233, 228)',
    },
    surfaceDisabled: 'rgba(25, 28, 27, 0.12)',
    onSurfaceDisabled: 'rgba(25, 28, 27, 0.38)',
    backdrop: 'rgba(41, 50, 48, 0.4)',
  },
};

const queryClient = new QueryClient();

function RootLayoutNav() {
  const router = useRouter();
  return (
    <QueryClientProvider client={queryClient}>
      <AuthProvider>
        <PaperProvider theme={theme}>
          <StatusBar style={Platform.OS === 'ios' ? 'light' : 'auto'} />

          <Stack>
            <Stack.Screen
              name="index" // Welcome Screen
              options={{
                title: '',
                headerTransparent: true,
                headerRight: () => (
                  <Link href="/discover" asChild>
                    <Button mode="outlined">Lewati</Button>
                  </Link>
                ),
              }}
            />
            <Stack.Screen name="(tabs)" options={{ headerShown: false }} />
            <Stack.Screen
              name="edit-account"
              options={{
                title: 'Ubah Akun',
                headerTitleAlign: 'center',
                headerLeft: () => (
                  <IconButton icon="chevron-left" onPress={router.back} />
                ),
              }}
            />
            <Stack.Screen name="login" options={{ headerShown: false }} />
            <Stack.Screen name="register" options={{ headerShown: false }} />
          </Stack>
        </PaperProvider>
      </AuthProvider>
    </QueryClientProvider>
  );
}
