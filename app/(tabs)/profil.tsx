import { ScrollView } from 'react-native';

import SafeAreaWrapper from '~components/SafeAreaWrapper';
import CardMenu from '~features/auth/components/ProfilCardMenu';
import CardSummary from '~features/auth/components/ProfilCardSummary';

export default function TabProfil() {
  return (
    <SafeAreaWrapper>
      <ScrollView>
        <CardSummary email="test1@test1.com" />
        <CardMenu />
      </ScrollView>
    </SafeAreaWrapper>
  );
}
