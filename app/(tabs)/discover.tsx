import { useCallback } from 'react';
import { RefreshControl, ScrollView } from 'react-native';

import SafeAreaWrapper from '~components/SafeAreaWrapper';
import AdsCarousel from '~features/discover/components/AdsCarousel';
import CouponBox from '~features/discover/components/CouponBox';
import Header from '~features/discover/components/Header';
import ListAroundYou from '~features/discover/components/ListAroundYou';
import ListCategory from '~features/discover/components/ListCategory';
import ListCheap10k from '~features/discover/components/ListCheap10k';
import ListFavourite from '~features/discover/components/ListFavourite';
import ListNearestRestaurant from '~features/discover/components/ListNearestRestaurant';
import { useListAds } from '~features/discover/hooks/ads';
import { useAroundYouQuery } from '~features/discover/hooks/aroundYou';
import { useCategoryQuery } from '~features/discover/hooks/category';
import { useCheap10kQuery } from '~features/discover/hooks/cheap10k';
import { useFavouriteQuery } from '~features/discover/hooks/favourite';
import { useNearestRestaurantQuery } from '~features/discover/hooks/nearestRestaurant';

export default function TabDiscover() {
  const categoryQuery = useCategoryQuery();
  const listAdsQuery = useListAds();
  const aroundYouQuery = useAroundYouQuery();
  const nearestRestaurantQuery = useNearestRestaurantQuery();
  const favouriteQuery = useFavouriteQuery();
  const cheap10kQuery = useCheap10kQuery();

  const refreshing =
    categoryQuery.isRefetching ||
    listAdsQuery.isRefetching ||
    aroundYouQuery.isRefetching ||
    nearestRestaurantQuery.isRefetching ||
    favouriteQuery.isRefetching ||
    cheap10kQuery.isRefetching;

  const onRefresh = useCallback(() => {
    categoryQuery.refetch();
    listAdsQuery.refetch();
    aroundYouQuery.refetch();
    nearestRestaurantQuery.refetch();
    favouriteQuery.refetch();
    cheap10kQuery.refetch();
  }, []);

  return (
    <SafeAreaWrapper>
      <ScrollView
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }>
        <Header />
        <AdsCarousel />
        <CouponBox />
        <ListCategory />
        <ListAroundYou />
        <ListNearestRestaurant />
        <ListFavourite />
        <ListCheap10k />
      </ScrollView>
    </SafeAreaWrapper>
  );
}
