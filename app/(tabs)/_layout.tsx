import { Link, Tabs } from 'expo-router';
import { useTheme } from 'react-native-paper';
import { StyleSheet } from 'react-native';

import Icon from '~components/Icon';

export default function TabLayout() {
  const theme = useTheme();

  return (
    <Tabs
      screenOptions={{
        tabBarActiveTintColor: theme.colors.primary,
      }}>
      <Tabs.Screen
        name="discover"
        options={{
          headerShown: false,
          tabBarIcon: ({ color }) => <Icon name="spoon" color={color} />,
        }}
      />
      <Tabs.Screen
        name="pesanan"
        options={{
          title: 'Pesanan',
          tabBarIcon: ({ color }) => <Icon name="shopping-bag" color={color} />,
        }}
      />
      <Tabs.Screen
        name="forum"
        options={{
          title: 'Forum',
          tabBarIcon: ({ color }) => <Icon name="comment-o" color={color} />,
        }}
      />
      <Tabs.Screen
        name="profil"
        options={{
          title: 'Profil',
          headerRight: () => (
            <Link href="edit-account" asChild style={styles.headerRightMargin}>
              <Icon name="gear" color="black" />
            </Link>
          ),
          tabBarIcon: ({ color }) => <Icon name="user" color={color} />,
        }}
      />
    </Tabs>
  );
}

const styles = StyleSheet.create({
  headerRightMargin: {
    marginHorizontal: 20,
  },
});
