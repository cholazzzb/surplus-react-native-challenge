/**
 * Navigation path that need auth
 */
export const Protected = new Set([
  'forum',
  'pesanan',
  'profil',
  'edit-account',
]);
