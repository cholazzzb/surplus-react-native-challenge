import { Link } from 'expo-router';

import XScrollable from '~components/XScrollable';
import CardFood from './CardFood';

import { useFavouriteQuery } from '../hooks/favourite';
import { Button, Text } from 'react-native-paper';

export default function ListFavourite() {
  const favouriteQuery = useFavouriteQuery();

  return (
    <XScrollable
      title="Paling disuka"
      subtitle="Nikmati menu terfavorit di Surplus!"
      rightComponent={
        <Link href="/" asChild>
          <Button mode="contained-tonal">
            <Text>Semua</Text>
          </Button>
        </Link>
      }
      isLoading={favouriteQuery.isLoading}
      listItems={favouriteQuery.data ?? []}
      component={(props) => <CardFood {...props} />}
    />
  );
}
