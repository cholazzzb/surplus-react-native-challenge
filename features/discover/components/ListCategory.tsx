import { Image } from 'expo-image';
import { StyleSheet } from 'react-native';

import XScrollable from '~components/XScrollable';
import { useCategoryQuery } from '../hooks/category';

export default function ListCategory() {
  const categoryQuery = useCategoryQuery();

  return (
    <XScrollable
      title="Kategori"
      isLoading={categoryQuery.isFetching}
      listItems={categoryQuery.data ?? []}
      component={(props) => (
        <Image source={props.imageUrl} style={styles.imageCategory} />
      )}
    />
  );
}

const styles = StyleSheet.create({
  imageCategory: {
    height: 100,
    width: 100,
    marginRight: 20,
  },
});
