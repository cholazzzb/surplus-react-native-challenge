import { Link } from 'expo-router';

import XScrollable from '~components/XScrollable';

import { useCheap10kQuery } from '../hooks/cheap10k';
import CardFood from './CardFood';
import { Button, Text } from 'react-native-paper';

export default function ListCheap10k() {
  const cheap10kQuery = useCheap10kQuery();

  return (
    <XScrollable
      title="Jajan hemat 10rb"
      subtitle="Semua menu serba hemat!"
      rightComponent={
        <Link href="/" asChild>
          <Button mode="contained-tonal">
            <Text>Semua</Text>
          </Button>
        </Link>
      }
      isLoading={cheap10kQuery.isLoading}
      listItems={cheap10kQuery.data ?? []}
      component={(props) => <CardFood {...props} />}
    />
  );
}
