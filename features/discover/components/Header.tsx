import { View, StyleSheet } from 'react-native';
import { Text, useTheme } from 'react-native-paper';

import Icon from '~components/Icon';

export default function Header() {
  const theme = useTheme();
  return (
    <View
      style={[
        styles.header,
        StyleSheet.flatten({ backgroundColor: theme.colors.primary }),
      ]}>
      <Text style={styles.textHeader}>Hi, Surplus Hero!</Text>
      <View style={styles.searchBar}>
        <Text>Mau selamatkan makanan apa hari ini?</Text>
        <Icon name="search" color="gray" />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  textHeader: {
    color: 'white',
    fontSize: 24,
    fontWeight: '700',
  },
  header: {
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    padding: 20,
    marginBottom: 50,
  },
  searchBar: {
    marginTop: 10,
    marginBottom: -50,
    backgroundColor: 'white',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 20,
    borderRadius: 10,
  },
});
