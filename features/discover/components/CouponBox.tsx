import { StyleSheet, View } from 'react-native';
import { Text, useTheme } from 'react-native-paper';

export default function CouponBox() {
  const theme = useTheme();
  return (
    <View
      style={[
        styles.couponRow,
        StyleSheet.flatten({ backgroundColor: theme.colors.outlineVariant }),
      ]}>
      <Text>Mau lihat voucher kamu? Yuk daftar!</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  couponRow: {
    borderRadius: 10,
    padding: 20,
    marginHorizontal: 20,
    flexDirection: 'row',
    justifyContent: 'center',
  },
});
