import { Link } from 'expo-router';

import XScrollable from '~components/XScrollable';
import CardFood from './CardFood';

import { useNearestRestaurantQuery } from '../hooks/nearestRestaurant';
import { Button, Text } from 'react-native-paper';

export default function ListNearestRestaurant() {
  const nearestRestaurantQuery = useNearestRestaurantQuery();

  return (
    <XScrollable
      title="Restauran terdekat"
      rightComponent={
        <Link href="/" asChild>
          <Button mode="contained-tonal">
            <Text>Semua</Text>
          </Button>
        </Link>
      }
      isLoading={nearestRestaurantQuery.isLoading}
      listItems={nearestRestaurantQuery.data ?? []}
      component={(props) => <CardFood {...props} />}
    />
  );
}
