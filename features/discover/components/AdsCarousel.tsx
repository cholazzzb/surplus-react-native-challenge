import { IconButton, Surface, Text } from 'react-native-paper';
import { StyleSheet } from 'react-native';

import Carousel from '~components/Carousel';
import { useListAds } from '~features/discover/hooks/ads';

export default function AdsCarousel() {
  const listAdsQuery = useListAds();
  const onPressReload = () => listAdsQuery.refetch();

  return !listAdsQuery.isError ? (
    <Carousel
      isLoading={listAdsQuery.isFetching}
      listItem={listAdsQuery.data ?? []}
    />
  ) : (
    <Surface style={styles.surface} elevation={4}>
      <Text variant="bodyMedium">Gagal Memuat, Coba lagi</Text>
      <IconButton
        icon="reload"
        size={20}
        mode="contained"
        onPress={onPressReload}
      />
    </Surface>
  );
}

const styles = StyleSheet.create({
  surface: {
    margin: 20,
    paddingVertical: 10,
    borderRadius: 20,
    backgroundColor: 'white',
    alignItems: 'center',
  },
});
