import { Link } from 'expo-router';

import XScrollable from '~components/XScrollable';
import CardFood from './CardFood';

import { useAroundYouQuery } from '../hooks/aroundYou';
import { Button, Text } from 'react-native-paper';

export default function ListAroundYou() {
  const aroundYouQuery = useAroundYouQuery();

  return (
    <XScrollable
      title="Di sekitar kamu"
      subtitle="Yuk selamatkan makanan di sekitarmu"
      rightComponent={
        <Link href="/" asChild>
          <Button mode="contained-tonal">
            <Text>Semua</Text>
          </Button>
        </Link>
      }
      isLoading={aroundYouQuery.isLoading}
      listItems={aroundYouQuery.data ?? []}
      component={(props) => <CardFood {...props} />}
    />
  );
}
