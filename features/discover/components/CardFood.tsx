import { StyleSheet, View } from 'react-native';
import { Image } from 'expo-image';

import { Badge, Text } from 'react-native-paper';

import { ChipVerified } from '~components/Chip';

import { Kilometer, Percent } from '../entity';

type Props = {
  id: string;
  foodImageUrl: string;
  restaurantImageUrl: string;
  distance: Kilometer;
  stock: number;
  foodName: string;
  availability: string;
  rating: number;
  sold: number;
  price: string;
  verified?: true;
  withDiscount?: true;
  discount: Percent;
  priceAfterDiscount: string;
};
export default function CardFood(props: Props) {
  return (
    <View style={styles.card}>
      <Image source={props.foodImageUrl} style={styles.imageFood} />
      <Image source={props.restaurantImageUrl} style={styles.imageRestaurant} />
      <View style={styles.viewDistance}>
        <Text>{props.distance} Km</Text>
      </View>
      {renderStock(props.stock)}
      <Text>{props.foodName}</Text>
      <Text style={styles.textAvailability}>{props.availability}</Text>
      <View style={styles.row}>
        {props.withDiscount && (
          <Badge
            style={styles.marginRight10}
            size={25}>{`${props.discount}%`}</Badge>
        )}
        <Text style={[props.withDiscount ? styles.textPriceStrikeThrough : {}]}>
          {props.price}
        </Text>
        {props.withDiscount && (
          <Text style={styles.marginLeft10}>{props.priceAfterDiscount}</Text>
        )}
      </View>
      <View style={styles.row}>
        <Text>{props.rating} | </Text>
        {renderSold(props.sold)}
      </View>
      {props.verified && <ChipVerified />}
    </View>
  );
}

const styles = StyleSheet.create({
  marginLeft10: {
    marginLeft: 10,
  },
  marginRight10: {
    marginRight: 10,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  card: {
    width: 160,
    marginHorizontal: 10,
  },
  imageFood: {
    width: '100%',
    height: 100,
    borderRadius: 5,
  },
  imageRestaurant: {
    width: 36,
    height: 36,
    borderRadius: 18,
    marginTop: -18,
    marginLeft: 9,
  },
  viewDistance: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: -43,
    marginBottom: 36,
    marginLeft: 95,
    backgroundColor: 'white',
    width: 60,
  },
  textAvailability: {
    fontSize: 10,
  },
  textPriceStrikeThrough: {
    textDecorationLine: 'line-through',
    textDecorationStyle: 'solid',
  },
});

const renderSold = (sold: number) => {
  if (sold < 5) {
    return <Text style={soldStyles.textNew}>Menu baru!</Text>;
  }
  return <Text style={soldStyles.text}>{sold}x terjual</Text>;
};

const soldStyles = StyleSheet.create({
  textNew: {
    color: 'blue',
    fontSize: 10,
  },
  text: {
    fontSize: 10,
  },
});

const renderStock = (stock: number) => {
  if (stock < 3) {
    return <Text>Hampir habis!</Text>;
  }

  if (stock < 10) {
    return <Text>{stock} Tersedia</Text>;
  }

  return <Text>10+ tersedia</Text>;
};
