import { createPriceStringFromNumber } from './logic';

describe('test', () => {
  it('should return 5.000', () => {
    const result = createPriceStringFromNumber(5000);
    expect(result).toBe('5.000');
  });

  it('should return 3.500', () => {
    const result = createPriceStringFromNumber(3500);
    expect(result).toBe('3.500');
  });
});
