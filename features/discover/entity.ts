export type Percent = number;
export type Kilometer = number;

export type Food = {
  foodImageUrl: string;
  restaurantImageUrl: string;
  distance: Kilometer;
  stock: number;
  foodName: string;
  availability: string;
  rating: number;
  sold: number;
  price: number;
  verified?: true;
  withDiscount?: true;
  discount: Percent;
  priceAfterDiscount: number;
};
