import { Food } from './entity';

export const createPriceStringFromNumber = (price: number) => {
  const mod = price % 1000;
  return mod > 0 ? `${Math.floor(price / 1000)}.${mod}` : `${price / 1000}.000`;
};

const randomFood = {
  sambal: ['Sambal Bawang', 'Sambal Terasi', 'Sambal Goang', 'Sambal Hijau'],
  bumbu: [
    'Bakar',
    'Goreng',
    'Panggang',
    'Keju',
    'Mentega',
    'Barbeque',
    'Pepes',
    'Blackpepper',
    'Lada',
    'Mayonaise',
  ],
  meats: ['Sapi', 'Ayam', 'Kambing', 'Telor'],
  main: ['Nasi', 'Steak', 'Sop'],
  cemilan: ['Roti', 'Donut', 'Risol', 'Kentang'],
};

const getRandom = <T extends string>(arr: Array<T>) => {
  const len = arr.length - 1;
  const idx = Math.round(Math.random() * len);
  return arr[idx];
};

const generateFoodName = () => {
  const variant = 2 + Math.round(Math.random());
  if (variant === 2) {
    const isCemilan = Math.random() < 0.5;

    return isCemilan
      ? `${getRandom(randomFood.cemilan)} ${getRandom(randomFood.bumbu)}`
      : `${getRandom(randomFood.meats)} ${getRandom(randomFood.bumbu)}`;
  }

  return `${getRandom(randomFood.main)} ${getRandom(
    randomFood.meats,
  )} ${getRandom(randomFood.sambal)}`;
};

export const createDummyFoods = (numOfFood = 10): Array<Food> => {
  const foods: Array<Food> = [];

  for (let idx = 0; idx < numOfFood; idx++) {
    const discount = Math.max(Math.floor(Math.random() * 10) * 10, 10);
    const priceNumber = 3000 + Math.floor(Math.random() * 100) * 500;
    const verified = Math.random() < 0.65; // 65%
    const withDiscount = Math.random() < 0.9; // 90%
    const newFood: Food = {
      foodImageUrl:
        'https://3.bp.blogspot.com/-NrcVsbZa0PM/VXCSmdRnj6I/AAAAAAAAABc/4Dfqub_ba3o/s1600/6.jpg',
      restaurantImageUrl:
        'https://img.favpng.com/18/23/8/restaurant-logo-png-favpng-wZNVya87WCYGbLLAGeSU0qJ9r.jpg',
      distance: Math.floor(Math.random() * 100) / 10,
      stock: Math.floor(Math.random() * 10),
      foodName: generateFoodName(),
      availability: 'Ambil hari ini, 06.00 - 20.00',
      discount,
      price: priceNumber,
      priceAfterDiscount: Math.ceil(((100 - discount) / 100) * priceNumber),
      rating: 3 + Math.floor(Math.random() * 20) / 10,
      sold: Math.random() < 0.8 ? Math.floor(Math.random() * 175) : 1,
    };
    if (verified) {
      newFood.verified = true;
    }
    if (withDiscount) {
      newFood.withDiscount = true;
    }
    foods.push(newFood);
  }

  return foods;
};
