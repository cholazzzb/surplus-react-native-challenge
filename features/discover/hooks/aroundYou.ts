import { useQuery } from '@tanstack/react-query';

import { getAroundYou } from '~features/discover/services/aroundYou';

export const useAroundYouQuery = () => {
  return useQuery({
    queryKey: ['list-around-you'],
    queryFn: getAroundYou,
  });
};
