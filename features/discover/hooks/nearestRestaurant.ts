import { useQuery } from '@tanstack/react-query';

import { getNearestRestaurant } from '~features/discover/services/nearestRestaurant';

export const useNearestRestaurantQuery = () => {
  return useQuery({
    queryKey: ['list-nearest-restaurant'],
    queryFn: getNearestRestaurant,
  });
};
