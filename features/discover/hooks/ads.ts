import { useQuery } from '@tanstack/react-query';

import { getListAds } from '~features/discover/services/ads';

export function useListAds() {
  return useQuery({
    queryKey: ['list-ads'],
    queryFn: getListAds,
    staleTime: Infinity,
  });
}
