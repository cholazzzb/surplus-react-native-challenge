import { useQuery } from '@tanstack/react-query';

import { getCheap10k } from '../services/cheap10k';

export const useCheap10kQuery = () => {
  return useQuery({
    queryKey: ['cheap-10k'],
    queryFn: getCheap10k,
  });
};
