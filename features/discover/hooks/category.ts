import { useQuery } from '@tanstack/react-query';

import { getCategories } from '~features/discover/services/category';

export const useCategoryQuery = () => {
  return useQuery({
    queryKey: ['category'],
    queryFn: getCategories,
  });
};
