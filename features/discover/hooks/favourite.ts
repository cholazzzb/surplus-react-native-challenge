import { useQuery } from '@tanstack/react-query';

import { getFavourite } from '../services/favourite';

export const useFavouriteQuery = () => {
  return useQuery({
    queryKey: ['list-favourite'],
    queryFn: getFavourite,
  });
};
