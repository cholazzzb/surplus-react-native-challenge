import mockApi from '~shared/api';
import shuffle, { createRandomByInterval } from '~shared/random';

export const getCategories = () => {
  return mockApi({
    respond: shuffle(
      mockRespond.map((cat) => ({
        id: `${Math.random() * 1000}random-${Math.random() * 1000}`,
        ...cat,
      })),
    ),
    delay: createRandomByInterval(),
  });
};

const mockRespond = [
  {
    imageUrl:
      'https://gitlab.com/cholazzzb/surplus-react-native-challenge/-/raw/main/static/category1.jpg',
  },
  {
    imageUrl:
      'https://gitlab.com/cholazzzb/surplus-react-native-challenge/-/raw/main/static/category2.jpg',
  },
  {
    imageUrl:
      'https://gitlab.com/cholazzzb/surplus-react-native-challenge/-/raw/main/static/category3.jpg',
  },
  {
    imageUrl:
      'https://gitlab.com/cholazzzb/surplus-react-native-challenge/-/raw/main/static/category4.jpg',
  },
  {
    imageUrl:
      'https://gitlab.com/cholazzzb/surplus-react-native-challenge/-/raw/main/static/category5.jpg',
  },
  {
    imageUrl:
      'https://gitlab.com/cholazzzb/surplus-react-native-challenge/-/raw/main/static/category6.jpg',
  },
  {
    imageUrl:
      'https://gitlab.com/cholazzzb/surplus-react-native-challenge/-/raw/main/static/category7.jpg',
  },
];
