import mockApi from '~shared/api';

import { Kilometer, Percent } from '../entity';
import { createDummyFoods, createPriceStringFromNumber } from '../logic';
import { createRandomByInterval } from '~shared/random';

export type FoodResponse = {
  id: string;
  foodImageUrl: string;
  restaurantImageUrl: string;
  distance: Kilometer;
  stock: number;
  foodName: string;
  availability: string;
  rating: number;
  sold: number;
  price: string;
  verified?: true;
  withDiscount?: true;
  discount: Percent;
  priceAfterDiscount: string;
};

export const getCheap10k = () => {
  return mockApi({
    respond: mockRespond,
    delay: createRandomByInterval(),
  });
};

const mockRespond: Array<FoodResponse> = createDummyFoods(30)
  .filter(
    (food) =>
      (food.withDiscount && food.priceAfterDiscount <= 10000) ||
      food.price <= 10000,
  )
  .map((food) => ({
    id: `${Math.random() * 1000}random-${Math.random() * 1000}`,
    ...food,
    price: createPriceStringFromNumber(food.price),
    priceAfterDiscount: createPriceStringFromNumber(food.priceAfterDiscount),
  }))
  .slice(0, 10);
