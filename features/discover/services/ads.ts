import mockApi from '~shared/api';
import shuffle, { createRandomByInterval } from '~shared/random';

type Ads = {
  imageUrl: string;
};
const mockRespond: Array<Ads> = [
  {
    imageUrl:
      'https://gitlab.com/cholazzzb/surplus-react-native-challenge/-/raw/main/static/ads1.png',
  },
  {
    imageUrl:
      'https://gitlab.com/cholazzzb/surplus-react-native-challenge/-/raw/main/static/ads2.png',
  },
  {
    imageUrl:
      'https://gitlab.com/cholazzzb/surplus-react-native-challenge/-/raw/main/static/ads3.png',
  },
];

export const getListAds = () => {
  return mockApi<Array<Ads>>({
    respond: shuffle(mockRespond),
    delay: createRandomByInterval(),
  });
};
