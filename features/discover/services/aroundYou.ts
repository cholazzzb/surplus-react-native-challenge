import mockApi from '~shared/api';

import { Kilometer, Percent } from '../entity';
import { createDummyFoods, createPriceStringFromNumber } from '../logic';
import { createRandomByInterval } from '~shared/random';

export type FoodResponse = {
  id: string;
  foodImageUrl: string;
  restaurantImageUrl: string;
  distance: Kilometer;
  stock: number;
  foodName: string;
  availability: string;
  rating: number;
  sold: number;
  price: string;
  verified?: true;
  withDiscount?: true;
  discount: Percent;
  priceAfterDiscount: string;
};

export const getAroundYou = () => {
  return mockApi({
    respond: mockRespond,
    delay: createRandomByInterval(),
  });
};

const mockRespond: Array<FoodResponse> = createDummyFoods().map((food) => ({
  id: `${Math.random() * 1000}random-${Math.random() * 1000}`,
  ...food,
  price: createPriceStringFromNumber(food.price),
  priceAfterDiscount: createPriceStringFromNumber(food.priceAfterDiscount),
}));
