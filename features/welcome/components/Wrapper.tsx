import { Link } from 'expo-router';
import { StyleSheet } from 'react-native';
import { Button, Text } from 'react-native-paper';

import BottomSheet from '~components/BottomSheet';

export default function Wrapper() {
  return (
    <BottomSheet>
      <>
        <Text variant="titleLarge" style={styles.textAlignCenter}>
          Selamat datang di Surplus
        </Text>
        <Text style={styles.textAlignCenter}>
          Selamatkan makanan berlebih di aplikasi Surplus agar tidak terbuang
          sia-sia
        </Text>
        <Link href="register" asChild>
          <Button mode="contained" style={styles.marginTop40}>
            Daftar
          </Button>
        </Link>
        <Link href="/login" asChild>
          <Button mode="outlined" style={styles.marginTop20}>
            Sudah punya akun? Masuk
          </Button>
        </Link>
        <Text style={[styles.textAlignCenter, styles.marginTop40]}>
          Dengan daftar atau masuk, Anda menerima syarat dan ketentuan serta
          kebijakan privasi
        </Text>
      </>
    </BottomSheet>
  );
}

const styles = StyleSheet.create({
  marginTop20: {
    marginTop: 20,
  },
  marginTop40: {
    marginTop: 40,
  },
  textAlignCenter: {
    textAlign: 'center',
  },
});
