import { UseMutationOptions, useMutation } from '@tanstack/react-query';

import {
  LoginRequest,
  LoginResponse,
  RegisterRequest,
  RegisterResponse,
  login,
  register,
} from './service';

export const useLogin = (
  options?: Omit<
    UseMutationOptions<LoginResponse, unknown, LoginRequest, unknown>,
    'mutationKey' | 'mutationFn'
  >,
) => {
  return useMutation(['login'], login, options);
};

export const useRegister = (
  options?: Omit<
    UseMutationOptions<RegisterResponse, unknown, RegisterRequest, unknown>,
    'mutationKey' | 'mutationFn'
  >,
) => {
  return useMutation(['register'], register, options);
};
