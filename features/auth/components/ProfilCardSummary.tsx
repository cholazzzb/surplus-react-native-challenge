import { Avatar, Card, Surface, Text } from 'react-native-paper';
import { StyleSheet } from 'react-native';

type Props = {
  email: string;
};
export default function CardSummary(props: Props) {
  return (
    <Surface style={styles.card}>
      <Avatar.Icon size={124} icon="account" />
      <Card.Content>
        <Text variant="titleLarge">Card title</Text>
        <Text>{props.email}</Text>
        <Text variant="bodyMedium">Card content</Text>
      </Card.Content>
      <Card.Actions>
        <Surface style={styles.surface}>
          <Text>Kamu menyelematkan </Text>
        </Surface>
        <Surface style={styles.surface}>
          <Text>Kamu Total Penghematan </Text>
        </Surface>
      </Card.Actions>
    </Surface>
  );
}

const styles = StyleSheet.create({
  card: {
    flexDirection: 'column',
    alignItems: 'center',
  },
  surface: {
    borderRadius: 20,
    padding: 20,
  },
});
