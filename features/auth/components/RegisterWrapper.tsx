import { zodResolver } from '@hookform/resolvers/zod';
import { Link, useRouter } from 'expo-router';
import { useState } from 'react';
import { Controller, useForm } from 'react-hook-form';
import { StyleSheet, View } from 'react-native';
import { Button, Text, TextInput, useTheme } from 'react-native-paper';
import { z } from 'zod';

import BottomSheet from '~components/BottomSheet';
import { useRegister } from '~features/auth/hook';

const formSchema = z
  .object({
    email: z
      .string()
      .email({ message: 'Harap masukkan alamat E-mail yang valid' }),
    password: z
      .string()
      .min(6, { message: 'Kata sandi kurang dari 6 karakter' }),
    confirmPassword: z.string(),
  })
  .refine((form) => form.password === form.confirmPassword, {
    message: 'Kata sandi tidak cocok',
    path: ['confirmPassword'],
  });

type FormSchema = z.infer<typeof formSchema>;
const initForm: FormSchema = {
  email: '',
  password: '',
  confirmPassword: '',
};

type Props = {
  onOpenModalLoading: () => void;
  onOpenModalError: () => void;
  onCloseModal: () => void;
};
const RegisterWrapper = (props: Props) => {
  const theme = useTheme();

  const [hidePassword, setHidePassword] = useState(true);
  const onToggleHidePassword = () => setHidePassword((prev) => !prev);

  const [confirmPassword, setConfirmPassword] = useState(true);
  const onToggleConfirmPassword = () => setConfirmPassword((prev) => !prev);

  const router = useRouter();

  const registerMutation = useRegister({
    onSuccess() {
      props.onCloseModal();
      router.replace('/login');
    },
    onError() {
      props.onOpenModalError();
    },
  });

  const {
    control,
    handleSubmit,
    formState: { isValid, errors },
  } = useForm<FormSchema>({
    resolver: zodResolver(formSchema),
    mode: 'all',
    defaultValues: initForm,
  });
  const onPressSubmit = handleSubmit((form) => {
    props.onOpenModalLoading();
    registerMutation.mutate(form);
  });

  return (
    <BottomSheet>
      <Text style={styles.textLabel}>E-mail</Text>
      <Controller
        control={control}
        name="email"
        render={({ field: { onChange, onBlur, value } }) => (
          <TextInput
            mode="outlined"
            placeholder="Alamat email kamu"
            keyboardType="email-address"
            error={!!errors?.email?.message}
            onBlur={onBlur}
            onChangeText={onChange}
            value={value}
          />
        )}
      />
      {errors.email?.message && (
        <Text style={styles.textError}>{errors.email.message}</Text>
      )}
      <Text style={[styles.textLabel, styles.marginTop12]}>Kata sandi</Text>
      <Controller
        control={control}
        name="password"
        render={({ field: { onChange, onBlur, value } }) => (
          <TextInput
            mode="outlined"
            placeholder="Masukkan kata sandi"
            error={!!errors?.email?.message}
            secureTextEntry={hidePassword}
            right={
              <TextInput.Icon
                icon={hidePassword ? 'eye' : 'eye-off'}
                onPress={onToggleHidePassword}
              />
            }
            onBlur={onBlur}
            onChangeText={onChange}
            value={value}
          />
        )}
      />
      {errors.password?.message && (
        <Text style={styles.textError}>{errors.password.message}</Text>
      )}
      <Text style={[styles.textLabel, styles.marginTop12]}>
        Ulangi kata sandi
      </Text>
      <Controller
        control={control}
        name="confirmPassword"
        render={({ field: { onChange, onBlur, value } }) => (
          <TextInput
            mode="outlined"
            placeholder="Ulangi kata sandi"
            error={!!errors?.email?.message}
            secureTextEntry={confirmPassword}
            right={
              <TextInput.Icon
                icon={confirmPassword ? 'eye' : 'eye-off'}
                onPress={onToggleConfirmPassword}
              />
            }
            onBlur={onBlur}
            onChangeText={onChange}
            value={value}
          />
        )}
      />
      {errors.confirmPassword?.message && (
        <Text style={styles.textError}>{errors.confirmPassword.message}</Text>
      )}
      <Button
        disabled={!isValid}
        style={styles.marginTop12}
        mode="contained"
        onPress={onPressSubmit}>
        Daftar
      </Button>
      <View style={[styles.row, styles.marginTop12]}>
        <View style={styles.divider} />
        <Text>Atau</Text>
        <View style={styles.divider} />
      </View>
      <View style={[styles.row, styles.spaceAround, styles.marginTop12]}>
        <Button mode="contained-tonal" icon="google">
          Google
        </Button>
      </View>
      <View style={[styles.viewPrivacy, styles.row, styles.marginTop12]}>
        <Text>Dengan mendaftar, Anda menerima </Text>
        <Text style={styles.textPrivacy}>syarat dan ketentuan </Text>
        <Text>serta </Text>
        <Text style={styles.textPrivacy}>kebijakan privasi</Text>
      </View>
      <View style={[styles.row, styles.marginTop12]}>
        <Text>Sudah punya akun? </Text>
        <Link href="/login">
          <Text
            style={[
              styles.textLogin,
              StyleSheet.flatten({ color: theme.colors.primary }),
            ]}>
            Yuk masuk
          </Text>
        </Link>
      </View>
    </BottomSheet>
  );
};

export default RegisterWrapper;

const styles = StyleSheet.create({
  marginTop12: {
    marginTop: 12,
  },
  textLabel: {
    fontWeight: '700',
  },
  textError: { color: 'red' },
  textLogin: {
    fontWeight: 'bold',
    textDecorationLine: 'underline',
  },
  textPrivacy: {
    color: 'orange',
    textDecorationLine: 'underline',
  },
  viewPrivacy: {
    width: '100%',
    flexWrap: 'wrap',
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  spaceAround: {
    justifyContent: 'space-around',
  },
  divider: {
    backgroundColor: 'gray',
    height: 2,
    flexGrow: 1,
    margin: 10,
  },
});
