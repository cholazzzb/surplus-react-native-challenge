import { zodResolver } from '@hookform/resolvers/zod';
import { Link, useRouter } from 'expo-router';
import { useState } from 'react';
import { Controller, useForm } from 'react-hook-form';
import { StyleSheet, View } from 'react-native';
import { Button, Text, TextInput, useTheme } from 'react-native-paper';
import { z } from 'zod';

import BottomSheet from '~components/BottomSheet';
import { useLogin } from '~features/auth/hook';
import { useAuth } from '~features/auth/provider';

const formSchema = z.object({
  email: z
    .string()
    .email({ message: 'Harap masukkan alamat E-mail yang valid' }),
  password: z.string().min(6, { message: 'Kata sandi kurang dari 6 karakter' }),
});
type FormSchema = z.infer<typeof formSchema>;
const initForm: FormSchema = {
  email: '',
  password: '',
};

type Props = {
  onOpenModalLoading: () => void;
  onOpenModalError: () => void;
  onCloseModal: () => void;
};
export default function Wrapper(props: Props) {
  const theme = useTheme();

  const [hidePassword, setHidePassword] = useState(true);
  const onToggleHidePassword = () => setHidePassword((prev) => !prev);

  const auth = useAuth();
  const router = useRouter();

  const loginMutation = useLogin({
    onSuccess(data) {
      auth.login(data);
      props.onCloseModal();
      router.replace('/discover');
    },
    onError() {
      props.onOpenModalError();
    },
  });

  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm<FormSchema>({
    resolver: zodResolver(formSchema),
    mode: 'all',
    defaultValues: initForm,
  });
  const onPressSubmit = handleSubmit((form) => {
    props.onOpenModalLoading();
    loginMutation.mutate(form);
  });

  return (
    <BottomSheet>
      <Text style={styles.textLabel}>E-mail</Text>
      <Controller
        control={control}
        name="email"
        render={({ field: { onChange, onBlur, value } }) => (
          <TextInput
            mode="outlined"
            placeholder="Alamat email kamu"
            keyboardType="email-address"
            error={!!errors?.email?.message}
            onBlur={onBlur}
            onChangeText={onChange}
            value={value}
          />
        )}
      />
      {errors.email?.message && (
        <Text style={styles.textError}>{errors.email.message}</Text>
      )}
      <Text style={[styles.textLabel, styles.marginTop12]}>Kata sandi</Text>
      <Controller
        control={control}
        name="password"
        render={({ field: { onChange, onBlur, value } }) => (
          <TextInput
            mode="outlined"
            placeholder="Masukkan kata sandi"
            error={!!errors?.password?.message}
            secureTextEntry={hidePassword}
            right={
              <TextInput.Icon
                icon={hidePassword ? 'eye' : 'eye-off'}
                onPress={onToggleHidePassword}
              />
            }
            onBlur={onBlur}
            onChangeText={onChange}
            value={value}
          />
        )}
      />
      {errors.password?.message && (
        <Text style={styles.textError}>{errors.password.message}</Text>
      )}
      <View style={[styles.row, styles.flexEnd, styles.marginTop12]}>
        <Text style={styles.textForgotPassword}>Lupa kata sandi?</Text>
      </View>
      <Button
        style={styles.marginTop12}
        mode="contained"
        onPress={onPressSubmit}>
        Masuk
      </Button>
      <View style={[styles.row, styles.marginTop12]}>
        <View style={styles.divider} />
        <Text>Atau</Text>
        <View style={styles.divider} />
      </View>
      <View style={[styles.row, styles.spaceAround, styles.marginTop12]}>
        <Button mode="contained-tonal" icon="facebook">
          Facebook
        </Button>
        <Button mode="contained-tonal" icon="google">
          Google
        </Button>
      </View>
      <View style={[styles.row, styles.marginTop12]}>
        <Text>Belum punya akun? </Text>
        <Link href="/register">
          <Text
            style={[
              styles.textRegister,
              StyleSheet.flatten({ color: theme.colors.primary }),
            ]}>
            Yuk daftar
          </Text>
        </Link>
      </View>
    </BottomSheet>
  );
}

const styles = StyleSheet.create({
  marginTop12: {
    marginTop: 12,
  },
  textLabel: {
    fontWeight: '700',
  },
  textError: { color: 'red' },
  textForgotPassword: {
    textDecorationLine: 'underline',
  },
  textRegister: {
    fontWeight: 'bold',
    textDecorationLine: 'underline',
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  spaceAround: {
    justifyContent: 'space-around',
  },
  flexEnd: {
    justifyContent: 'flex-end',
  },
  divider: {
    backgroundColor: 'gray',
    height: 2,
    flexGrow: 1,
    margin: 10,
  },
});
