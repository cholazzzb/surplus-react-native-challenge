import { StyleSheet } from 'react-native';
import { List, Surface } from 'react-native-paper';

export default function CardMenu() {
  return (
    <Surface style={styles.surface} elevation={4}>
      <List.Item
        title="Voucher saya"
        description="Lihat voucher kamu yang sedang aktif saat ini"
        left={(props) => <List.Icon {...props} icon="cash-100" />}
      />
      <List.Item
        title="Voucher Saya"
        description="Berikan ulasan kepada penjual makanan yang sudah kamu beli"
        left={(props) => <List.Icon {...props} icon="chat" />}
      />
      <List.Item
        title="Dampak Infografik"
        description="Cari tahu seberapa besar makanan yang telah kamu selamatkan lewat aplikasi Surplus"
        left={(props) => <List.Icon {...props} icon="chart-pie" />}
      />
      <List.Item
        title="Komunitas Surplus"
        description="Bergabung dengan tim komunitas Surplus untuk memerangi masalah food waste di Indonesia"
        left={(props) => <List.Icon {...props} icon="account-group" />}
      />
      <List.Item
        title="Surplus Partner"
        description="Perangi food waste dengan menjadi mitra kami lewat aplikasi Surplus Partner"
        left={(props) => <List.Icon {...props} icon="handshake" />}
      />
      <List.Item
        title="Nilai Aplikasi Surplus"
        description="Berikan rating dan pengalaman kamu untuk aplikasi Surplus"
        left={(props) => <List.Icon {...props} icon="hand-heart" />}
      />
    </Surface>
  );
}

const styles = StyleSheet.create({
  surface: {
    marginTop: 20,
    backgroundColor: 'white',
  },
});
