export type Email = string;
export type Password = string;

export type User = {
  token: string;
};
