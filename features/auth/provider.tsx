import { useRouter, useSegments } from 'expo-router';
import React, { PropsWithChildren } from 'react';

import { User } from './entity';
import { Protected } from '~constants/Auth';

type Context = {
  login: (user: User) => void;
  logout: () => void;
  user: User;
};

const initContext: Context = {
  login: () => {
    //
  },
  logout: () => {
    //
  },
  user: {
    token: '',
  },
};
const AuthContext = React.createContext<Context>(initContext);

export const useAuth = () => {
  return React.useContext(AuthContext);
};

const useProtectedRoute = (user: { token: string }) => {
  const segments = useSegments();
  const router = useRouter();

  React.useEffect(() => {
    const len = segments.length;
    const inAuthGroup = Protected.has(segments[len - 1]);

    if (!user?.token && inAuthGroup) {
      router.replace('/login');
    }
  }, [user, segments]);
};

export const AuthProvider = (props: PropsWithChildren) => {
  const [user, setAuth] = React.useState<{ token: string }>(initContext.user);

  useProtectedRoute(user);

  return (
    <AuthContext.Provider
      value={{
        login: (form) => setAuth(form),
        logout: () => setAuth(initContext.user),
        user,
      }}>
      {props.children}
    </AuthContext.Provider>
  );
};
