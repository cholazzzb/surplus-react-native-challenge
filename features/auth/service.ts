import mockApi from '~shared/api';
import { createRandomByInterval } from '~shared/random';
import { Email, Password, User } from './entity';

export const getProfile = () => {
  return mockApi({
    respond: mockRespond,
  });
};

const mockRespond = {
  email: 'test1@test1.com',
};

// dummy Database
const registeredUsers: Record<Email, Password> = {
  'test1@test1.com': 'password',
};

export type LoginRequest = {
  email: Email;
  password: Password;
};
export type LoginResponse = User;
export const login = (req: LoginRequest): Promise<LoginResponse> => {
  return new Promise((res, rej) => {
    setTimeout(() => {
      if (registeredUsers[req.email] === req.password) {
        res({
          token: 'magicToken',
        });
      } else {
        rej('user not found');
      }
    }, 1000);
  });
};

export type RegisterRequest = {
  email: Email;
  password: Password;
  confirmPassword: Password;
};
export type RegisterResponse = string;
export const register = (req: RegisterRequest): Promise<RegisterResponse> => {
  const delay = createRandomByInterval();
  return new Promise((res, rej) => {
    setTimeout(() => {
      if (registeredUsers[req.email]) {
        rej('User already registered');
      } else {
        registeredUsers[req.email] = req.password;
        res('Success');
      }
    }, delay);
  });
};
