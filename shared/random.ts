export default function shuffle<T>(arr: Array<T>) {
  const result = [...arr];

  for (let idx = arr.length - 1; idx > 0; idx--) {
    const randomIdx = Math.round(Math.random() * idx);
    [result[randomIdx], result[idx]] = [result[idx], result[randomIdx]];
  }

  return result;
}

export const createRandomByInterval = (min = 1000, max = 3000) => {
  return Math.max(min, Math.random() * max);
};
