type Miliseconds = number;
type Percent = number;
type Params<T> = {
  respond: T;
  successProbability?: Percent;
  delay?: Miliseconds;
};
export default function mockApi<T extends object | Array<object>>({
  respond,
  successProbability = 95,
  delay = 1000,
}: Params<T>): Promise<T> {
  return new Promise((res, rej) => {
    setTimeout(() => {
      if (Math.random() < successProbability / 100) {
        res(respond);
      } else {
        rej('Internal Server Error');
      }
    }, delay);
  });
}
