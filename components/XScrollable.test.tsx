import { Button, Text } from 'react-native-paper';
import renderer from 'react-test-renderer';

describe('Should render correctly', () => {
  it('renders the correct text', () => {
    const inst = renderer.create(
      <Button>
        <Text>example</Text>
      </Button>,
    );
    const textInst = inst.root.findByType(Text);
    expect(textInst.children.length).toEqual(1);
  });
});
