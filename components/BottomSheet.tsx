import { StyleProp, StyleSheet, View, ViewStyle } from 'react-native';
import { PropsWithChildren } from 'react';

export default function BottomSheet(
  props: PropsWithChildren<{ style?: StyleProp<ViewStyle> }>,
) {
  return (
    <View style={[styles.bottomSheet, props?.style]}>{props.children}</View>
  );
}

const styles = StyleSheet.create({
  bottomSheet: {
    backgroundColor: 'white',
    borderTopStartRadius: 20,
    borderTopEndRadius: 20,
    padding: 40,
  },
});
