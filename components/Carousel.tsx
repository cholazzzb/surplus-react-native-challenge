import { Image } from 'expo-image';
import { useEffect, useRef, useState } from 'react';
import {
  Animated,
  Dimensions,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  View,
} from 'react-native';
import { useTheme } from 'react-native-paper';

import Skeleton from './Skeleton';

const CARD_OFFSET = 37;
const WINDOW_WIDTH = Dimensions.get('window').width;
const CARD_WIDTH = WINDOW_WIDTH - CARD_OFFSET * 2;

type MandatoryListItem = {
  imageUrl: string;
};

type Props<T> = {
  isLoading: boolean;
  listItem: Array<T & MandatoryListItem>;
};
export default function Carousel<T extends object>(props: Props<T>) {
  const theme = useTheme();
  const paginationActiveStyle = [
    styles.paginationDotActive,
    StyleSheet.flatten({
      backgroundColor: theme.colors.primary,
    }),
  ];

  const [selectedItem, setSelectedItem] = useState(0);
  const scrollRef = useRef<ScrollView>(null);
  const scrollX = useRef(new Animated.Value(0)).current;

  const timerRef = useRef<NodeJS.Timeout>();

  useEffect(() => {
    const changeItem = () => {
      timerRef.current = setTimeout(() => {
        setSelectedItem((prevIdx) => {
          const nextIdx = (prevIdx + 1) % props.listItem.length;

          scrollRef.current?.scrollTo({
            x: CARD_WIDTH * nextIdx,
            animated: true,
          });

          return nextIdx;
        });
        changeItem();
      }, 2000);
    };

    changeItem();

    return () => {
      if (timerRef.current) {
        clearTimeout(timerRef.current);
      }
    };
  }, [scrollRef.current, props.listItem.length]);

  return (
    <>
      <ScrollView
        horizontal
        ref={scrollRef}
        decelerationRate="normal"
        snapToInterval={CARD_WIDTH}
        showsHorizontalScrollIndicator={false}
        bounces={false}
        disableIntervalMomentum
        onScroll={Animated.event(
          [{ nativeEvent: { contentOffset: { x: scrollX } } }],
          { useNativeDriver: false },
        )}
        onMomentumScrollEnd={(event) => {
          const selected = Math.floor(
            (event.nativeEvent.contentOffset.x + 10) / CARD_WIDTH,
          );
          if (selected !== selectedItem) {
            setSelectedItem(selected);
          }
        }}
        scrollEventThrottle={12}>
        {props.isLoading &&
          Array(5)
            .fill(undefined)
            .map((_, idx) => (
              <Skeleton
                key={`carousel-skeleton-${idx}`}
                width={CARD_WIDTH}
                height={200}
                style={styles.skeleton}
              />
            ))}
        {!props.isLoading &&
          props.listItem.map((item, idx) => {
            const inputRange = [
              (idx - 1) * CARD_WIDTH,
              idx * CARD_WIDTH,
              (idx + 1) * CARD_WIDTH,
            ];

            const translate = scrollX.interpolate({
              inputRange,
              outputRange: [0.87, 1, 0.87],
            });

            const opacity = scrollX.interpolate({
              inputRange,
              outputRange: [0.7, 1, 0.7],
            });

            const style = {
              opacity,
              transform: [{ scale: translate }],
              marginLeft: idx === 0 ? CARD_OFFSET : 0,
              marginRight: idx === props.listItem.length - 1 ? CARD_OFFSET : 0,
            };

            return (
              <Animated.View
                key={`product-${idx}`}
                style={[styles.card, style]}>
                <Image
                  source={{
                    uri: item.imageUrl,
                  }}
                  style={styles.cardImage}
                />
              </Animated.View>
            );
          })}
      </ScrollView>
      {!props.isLoading && (
        <View style={styles.pagination}>
          {props.listItem.map((_, idx) => {
            const isSelected = idx === selectedItem;
            const onPress = () => {
              scrollRef.current?.scrollTo({
                x: CARD_WIDTH * idx,
                animated: true,
              });
              setSelectedItem(idx);
            };

            return (
              <TouchableOpacity
                key={`dot-${idx}`}
                style={
                  isSelected
                    ? paginationActiveStyle
                    : styles.paginationDotNotActive
                }
                onPress={onPress}
              />
            );
          })}
        </View>
      )}
    </>
  );
}

const styles = StyleSheet.create({
  skeleton: {
    marginHorizontal: CARD_OFFSET,
    marginVertical: 20,
  },
  card: {
    width: CARD_WIDTH,
    marginVertical: -70,
  },
  cardImage: {
    contentFit: 'contain',
    aspectRatio: 1,
  },
  pagination: {
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: CARD_OFFSET,
    marginVertical: 25,
  },
  paginationDotActive: {
    width: 10,
    height: 10,
    borderRadius: 5,
    marginHorizontal: 4,
  },
  paginationDotNotActive: {
    width: 10,
    height: 10,
    backgroundColor: 'gray',
    marginHorizontal: 4,
    borderRadius: 5,
  },
});
