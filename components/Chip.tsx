import { StyleSheet } from 'react-native';
import { Chip } from 'react-native-paper';

export const ChipVerified = () => {
  return <Chip textStyle={styles.verified}>Sudah terverifikasi Surplus</Chip>;
};

const styles = StyleSheet.create({
  verified: { fontSize: 10 },
});
