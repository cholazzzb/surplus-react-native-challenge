import { PropsWithChildren } from 'react';
import {
  Platform,
  SafeAreaView,
  View,
  StyleSheet,
  StyleProp,
  ViewStyle,
} from 'react-native';

export default function SafeAreaWrapper({
  style,
  children,
}: PropsWithChildren<{
  style?: StyleProp<ViewStyle>;
}>) {
  return Platform.OS === 'ios' ? (
    <SafeAreaView>{children}</SafeAreaView>
  ) : (
    <View style={[styles.paddingTop20, style]}>{children}</View>
  );
}

const styles = StyleSheet.create({
  paddingTop20: {
    paddingTop: 20,
  },
});
