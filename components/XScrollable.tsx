import { FunctionComponent } from 'react';
import { FlatList, StyleSheet, View } from 'react-native';
import { Text } from 'react-native-paper';

import Skeleton from './Skeleton';

type Props<T> = {
  title: string;
  subtitle?: string;
  rightComponent?: React.ReactElement;
  isLoading: boolean;
  listItems: Array<T & { id: string }>;
  component: FunctionComponent<T>;
};
export default function XScrollable<T extends object>(props: Props<T>) {
  return (
    <View style={styles.viewContainer}>
      <View style={styles.header}>
        <View>
          <Text style={styles.textHeader}>{props.title}</Text>
          {props.subtitle && <Text>{props.subtitle}</Text>}
        </View>
        {props.rightComponent && props.rightComponent}
      </View>
      <FlatList
        horizontal
        showsHorizontalScrollIndicator={false}
        data={!props.isLoading ? props.listItems : []}
        contentContainerStyle={styles.flatListContent}
        ListEmptyComponent={() => {
          return (
            <>
              {Array(10)
                .fill(null)
                .map((_, idx) => (
                  <Skeleton key={`skeleton-${idx}`} style={styles.skeleton} />
                ))}
            </>
          );
        }}
        keyExtractor={(item) => item.id}
        renderItem={({ item }) => <props.component key={item.id} {...item} />}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  viewContainer: {
    marginBottom: 30,
  },
  skeleton: {
    marginHorizontal: 10,
  },
  row: {
    flexDirection: 'row',
  },
  header: {
    flexDirection: 'row',
    padding: 10,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  textHeader: {
    fontWeight: '700',
  },
  flatListContent: {
    marginHorizontal: 10,
  },
});
