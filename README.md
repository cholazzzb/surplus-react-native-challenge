# Surplus React Native Challenge

# Init project by
1. ```npx create-expo-app --template```

# How to setup
1. Make sure you have node.js, react-native, android studio, npm and yarn installed
2. clone this repo
3. yarn install
4. yarn android
NOTES: ios not tested, because developed on windows

Happy coding :)

# To Build APK: (Need Expo account)
```yarn build-apk```

# Features:
1. Mock Login and Register (without OTP), Profil and Logout with some error handling
2. Mock Explore Backend Service with Accurrate Random Data (Restaurant Data: Jarak <3km, Paling Disuka: Rating >4.2, Jajan Hemat 10rb: Harga <10rb) + some error handling
3. Protect Screen if user not login
4. Better Developer Experience with auto generate Import Image when new image is put on the assets/image directory
5. Better Developer Experience with `~` prefix for import module
6. Use Git Hooks to improve code quality and better consistency of code style: variable type, styling, strict usage with eslint

## Features Animations (Please wait, the size is quite big):
Register | Logout | Error on Login | Protect if not login | Discover 
--- | --- | --- | --- |--- 
<img src="https://gitlab.com/cholazzzb/surplus-react-native-challenge/-/raw/main/static/animated/register.gif" width="50%" height="50%"/> | <img src="https://gitlab.com/cholazzzb/surplus-react-native-challenge/-/raw/main/static/animated/logout.gif" width="50%" height="50%"/> | <img src="https://gitlab.com/cholazzzb/surplus-react-native-challenge/-/raw/main/static/animated/error-on-login.gif" width="50%" height="50%"/> | <img src="https://gitlab.com/cholazzzb/surplus-react-native-challenge/-/raw/main/static/animated/protect-if-not-login.gif" width="50%" height="50%"/> | <img src="https://gitlab.com/cholazzzb/surplus-react-native-challenge/-/raw/main/static/animated/discover.gif" width="50%" height="50%"/>

# Download APK and install the app
To Download APK, scan this QR
<img src="https://gitlab.com/cholazzzb/surplus-react-native-challenge/-/raw/main/static/animated/QR.jpg" width="50%" height="50%"/>
or open this link
https://expo.dev/accounts/toroambis/projects/surplus-react-native-challenge/builds/7eb9bfda-01c9-479c-ac99-1487372cafde

Please Note that, for some security reason your device will prevent to install this app. So you need to ignore it.


# NOTES:
1. Developed on windows so only android are worked. ios is not checked
2. This project use the new `expo-router` and currently `expo-router` has a bug and will give warning, see this `https://github.com/expo/router/issues/278`. In short the warning you get when run this project is from expo-router

# Tech Stack
1. React Native with Typescript
2. Expo & Expo Router
3. Jest
4. React Test Renderer
5. React Native Paper
6. React Query
7. React Hook Form

# Tools
Eslint, prettier, typescript, husky, zod, react-hook-form


# User for login
#### email: `test1@test1.com`

#### password: `password`

Show case your React Native skills!

# Steps and Procedure

1. Download Surplus app in Play Store and/or App Store
2. Check and use the app
3. Develop first similar flows (Splash screen + login/register + random mocked first screen, you can add more, the more merrier) using React Native, use open stock screens examples (or use your owned ones)
4. After developing, when you are submitting notify us by email, please also include in your email your impression and review of our Surplus App (Honest Review will be appreciated)
5. (not mandatory) Give a glimpse of your developed mobile app in a screen recording and short explanations

# App Developemnt Criteria

1. Fork this repository into your public repository (you may use github if you want to, but please notify us)
2. Push, commit many times in the forked repo as you want, but also consider meaningful commit names, time will be counted right after your init commit
3. Add /firmanserdana as contributors
4. Update readme as how to use the code and app (will be checked blindly, so ensure pre/post-steps/configuration will also be provided)
5. Development last step/commit => create a merge request into the main branch, add /firmanserdana as the reviewer 

## Marking and Plus Points
This list is just a guidance, if it's not possible to do the mandatory list, please note it into your readme
1. (Mandatory) Use Open Public API for functional stuff in the random mocked first screen (e.g. weather info at http://www.7timer.info/doc.php?lang=en)
2. (Mandatory) Login credential can be hardcoded, but better to also show case all possible scenarios (success login, failed login, success registering etc.)
3. (Mandatory) App-Styles will be totally considered, make it as beatiful as it can be
4. (Mandaory) Consider multiple phone layouts (the more merrier, but please tell us what phones you are using for developing the app)
5. (Plus Point) ensure cross platform usablity (Android and iOS)
6. (Plus Point) you can use other tech stacks if you want, e.g. Flutter
7. (Plus Point) provide unit test and write the procedures in th readme.
